mod intcode;

fn main() {
    let mut program = intcode::read_from_stdin();
    let result = intcode::process(&mut program.clone(), 1);
    println!("Day 5 part 1: {}", result);
    let result = intcode::process(&mut program, 5);
    println!("Day 5 part 2: {}", result);
}
