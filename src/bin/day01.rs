use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let weights: Vec<i32> = stdin.lock().lines()
        .map(|l| l.unwrap().parse::<i32>().unwrap())
        .collect();

    println!("Part 1: {}", weights.iter().map(|n| n / 3 -2).sum::<i32>());

    let mut required = 0;
    for n in weights.iter() {
        let mut current = n / 3 - 2;
        while current > 0 {
            required += current;
            current = current / 3 - 2;
        }
    }
    println!("Part 2: {}", required);
}
