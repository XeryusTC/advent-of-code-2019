use std::collections::HashMap;

mod intcode;

type Position = (intcode::IntCode, intcode::IntCode);

fn main() {
    let program = intcode::State::new(&intcode::read_from_stdin());
    let mut part1 = program.clone();
    let mut screen: HashMap<Position, intcode::IntCode> = HashMap::new();

    part1.process(None);
    for i in 0..(part1.outputs.len() / 3) {
        let coord = (part1.outputs[i * 3], part1.outputs[i * 3 + 1]);
        let pixel = screen.entry(coord).or_insert(0);
        *pixel = part1.outputs[i * 3 + 2];
    }
    println!("Day 13 part 1: {}", screen.values()
             .filter(|v| **v == 2).count());
}
