use std::collections::HashSet;

mod intcode;

// Taken from Rosetta code
pub fn permutations(size: usize) -> Permutations {
    Permutations { idxs: (0..size).collect(), swaps: vec![0; size], i: 0 }
}
 
pub struct Permutations {
    idxs: Vec<usize>,
    swaps: Vec<usize>,
    i: usize,
}
 
impl Iterator for Permutations {
    type Item = Vec<usize>;
 
    fn next(&mut self) -> Option<Self::Item> {
        if self.i > 0 {
            loop {
                if self.i >= self.swaps.len() { return None; }
                if self.swaps[self.i] < self.i { break; }
                self.swaps[self.i] = 0;
                self.i += 1;
            }
            self.idxs.swap(self.i, (self.i & 1) * self.swaps[self.i]);
            self.swaps[self.i] += 1;
        }
        self.i = 1;
        Some(self.idxs.clone())
    }
}

fn main() {
    let program = intcode::read_from_stdin();
    //let result = permutations(5).map(|perm| {
        //let mut last = 0;
        //for phase in perm {
            //last = intcode::process(&mut program.clone(),
                                    //&mut vec![last, phase as isize]).unwrap();
        //}
        //last
    //}).max().unwrap();
    //println!("Day 7 part 1: {}", result

    let result = permutations(5).map(|perm| {
        let perm = perm.iter().map(|p| p + 5).collect::<Vec<usize>>();
        let mut last = 0;
        let mut amps = perm.iter().map(|p| {
            let mut amp = intcode::State::new(&program);
            amp.process(Some(*p as isize));
            amp
        }).collect::<Vec<intcode::State>>();
        while !amps.iter().any(|a| match a.state {
                intcode::RunningState::Done => true,
                _ => false,
        }) {
            for amp in amps.iter_mut() {
                last = amp.process(Some(last)).unwrap();
            }
        }
        last
    }).max().unwrap();
    println!("Day 7 part 2: {}", result);
}
