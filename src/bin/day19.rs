mod intcode;

fn main() {
    let program = intcode::read_from_stdin();
    let in_beam: isize = (0..50).flat_map(|x| {
        (0..50).map(|y| {
             intcode::process(&mut program.clone(),
                              &mut vec![x, y]).unwrap()
        }).collect::<Vec<isize>>()
    }).sum();
    println!("Day 19 part 1: {}", in_beam);

    // Part 2
    for y in 1000..10_000 {
        // Find the left part of the tractor beam
        let mut min_x = 0;
        for x in 0..10_000 {
            let result = intcode::process(&mut program.clone(),
                                          &mut vec![x, y]).unwrap();
            if result == 1 {
                min_x = x;
                break;
            }
        }

        if intcode::process(&mut program.clone(),
                            &mut vec![min_x + 99, y - 99]).unwrap() == 1 {
            println!("Day 19 part 2: {}", (y - 99) * 10000 + min_x);
            break;
        }
    }
}
