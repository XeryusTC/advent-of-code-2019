use std::io::{self, BufRead};

fn process_with_replace(codes: &Vec<usize>, noun: usize, verb: usize)
    -> usize
{
    let mut codes = codes.clone();
    codes[1] = noun;
    codes[2] = verb;
    process(&mut codes)
}

fn process(codes: &mut Vec<usize>) -> usize {
    let mut p = 0;
    while codes[p] != 99 {
        let a = codes[p + 1];
        let b = codes[p + 2];
        let c = codes[p + 3];
        match codes[p] {
            1 => codes[c] = codes[a] + codes[b],
            2 => codes[c] = codes[a] * codes[b],
            n => panic!("unknown opcode {} found", n)
        }
        p += 4;
    }
    codes[0]
}

fn main() {
    let stdin = io::stdin();
    let mut codes: Vec<usize> = stdin.lock().lines().next().unwrap().unwrap()
        .split(',').map(|v| v.parse::<usize>().unwrap()).collect();
    codes[1] = 12;
    codes[2] = 2;

    println!("Part 1: {}", process_with_replace(&codes, 12, 2));

    'outer: for noun in 0..100 {
        'inner: for verb in 0..100 {
            let result = process_with_replace(&codes, noun, verb);
            if result == 19690720 {
                println!("Part 2: {}", noun * 100 + verb);
                break 'outer;
            }
        }
    }
}
