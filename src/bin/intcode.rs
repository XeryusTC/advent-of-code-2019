#![allow(dead_code)]

use std::io::{self, BufRead};

pub type IntCode = isize;
pub type Program = Vec<IntCode>;

#[derive(Debug)]
enum Mode {
    Position,
    Immediate,
    Relative,
}

impl Mode {
    fn from_opcode(opcode: &IntCode, argument_num: &usize) -> Mode {
        match opcode / 10_isize.pow(*argument_num as u32 + 1) % 10 {
            0 => Mode::Position,
            1 => Mode::Immediate,
            2 => Mode::Relative,
            _ => Mode::Position,
        }
    }
}

#[derive(Clone, Debug)]
pub enum RunningState {
    Start,
    WaitingForInput,
    Done,
}

#[derive(Clone, Debug)]
pub struct State {
    program: Program,
    pointer: usize,
    relative_base: IntCode,
    pub output: Option<IntCode>,
    pub outputs: Vec<IntCode>,
    pub state: RunningState,
}

impl State {
    pub fn new(program: &Program) -> State {
        State {
            program: program.clone(),
            pointer: 0,
            relative_base: 0,
            output: None,
            outputs: vec![],
            state: RunningState::Start,
        }
    }

    pub fn get_value(&mut self, argument: usize) -> IntCode {
        let mode = Mode::from_opcode(&self.program[self.pointer], &argument);
        let position: usize = match mode {
            Mode::Position  => self.program[self.pointer + argument] as usize,
            Mode::Immediate => self.pointer + argument,
            Mode::Relative  => {
                (self.relative_base +
                    self.program[self.pointer + argument]) as usize
            }
        };
        if position >= self.program.len() {
            self.program.resize(position + 1, 0);
        }
        self.program[position]
    }

    pub fn write(&mut self, argument: usize, value: &IntCode) {
        let mode = Mode::from_opcode(&self.program[self.pointer], &argument);
        let position: usize = match mode {
            Mode::Relative => (self.relative_base +
                self.program[self.pointer + argument]) as usize,
            Mode::Position | Mode::Immediate =>
                self.program[self.pointer + argument] as usize,
        };
        if position >= self.program.len() {
            self.program.resize(position + 1, 0);
        }
        self.program[position] = *value;
    }

    pub fn process(&mut self, mut input: Option<IntCode>) -> Option<IntCode> {
        let mut running = true;

        while running {
            let mut skip = true;
            let opcode = self.program[self.pointer];
            //println!("Executing {}", self.pointer);
            //print_debug_program(&self.program);
            match opcode % 100 {
                1 => { // add
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    self.write(3, &(a + b));
                }
                2 => { // multiply
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    self.write(3, &(a * b));
                }
                3 => { // input
                    match input {
                        Some(i) => {
                            self.write(1, &i);
                            input = None;
                        }
                        None => {
                            self.state = RunningState::WaitingForInput;
                            return self.output;
                        }
                    }
                }
                4 => { // output
                    let output = self.get_value(1);
                    self.output = Some(output);
                    self.outputs.push(output);
                }
                5 => { // jump-if-true
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    if a != 0 {
                        skip = false;
                        self.pointer = b as usize;
                    }
                }
                6 => { // jump-if-false
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    if a == 0 {
                        skip = false;
                        self.pointer = b as usize;
                    }
                }
                7 => { // less than
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    self.write(3, &if a < b { 1 } else { 0 });
                }
                8 => { // equals
                    let a = self.get_value(1);
                    let b = self.get_value(2);
                    self.write(3, &if a == b { 1 } else { 0 });
                }
                9 => { // adjust relative base
                    let a = self.get_value(1);
                    self.relative_base += a;
                }
                99 => { // halt
                    running = false;
                    self.state = RunningState::Done;
                }
                n => panic!("Unknown opcode {} in position {}", n,
                            self.pointer),
            }
            if skip {
                self.pointer += determine_skip(&self.program[self.pointer]);
            }
        }
        self.output
    }

    pub fn reset_output(&mut self) {
        self.outputs = vec![];
    }
}

pub fn process(program: &mut Program,
               inputs: &mut Vec<isize>,
              ) -> Option<IntCode>
{
    let mut state = State::new(program);
    loop {
        state.process(inputs.pop());
        if let RunningState::Done = state.state {
            break;
        }
    }
    state.output
}

pub fn read_from_stdin() -> Program {
    let stdin = io::stdin();
    let program = stdin.lock().lines().next().unwrap().unwrap()
        .split(',')
        .map(|v| v.parse::<IntCode>().unwrap())
        .collect();
    program
}

pub fn print_debug_program(program: &Program) {
    let mut i = 0;
    while i < program.len() {
        print!("{:>3}:", i);
        let size = determine_skip(&program[i]);
        for j in 0..size {
            if i + j >= program.len() {
                break;
            }
            print!(" {}", program[i + j]);
        }
        println!("");
        i += size;
    }
}

fn determine_skip(opcode: &IntCode) -> usize {
    match opcode % 100 {
        1 => 4,
        2 => 4,
        3 => 2,
        4 => 2,
        5 => 3,
        6 => 3,
        7 => 4,
        8 => 4,
        9 => 2,
        _ => 1,
    }
}
