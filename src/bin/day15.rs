use std::collections::HashMap;

mod intcode;

const NORTH: intcode::IntCode = 1;
const SOUTH: intcode::IntCode = 2;
const WEST:  intcode::IntCode = 3;
const EAST:  intcode::IntCode = 4;

const WALL: intcode::IntCode = 0;
const OPEN: intcode::IntCode = 1;
const OXYGEN: intcode::IntCode = 2;

type Map = HashMap<Position, Tile>;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Position {
    x: isize,
    y: isize,
}

impl Position {
    fn neighbour(&self, direction: intcode::IntCode) -> Position {
        match direction {
            NORTH => Position { x: self.x,     y: self.y - 1},
            SOUTH => Position { x: self.x,     y: self.y + 1},
            WEST  => Position { x: self.x - 1, y: self.y},
            EAST  => Position { x: self.x + 1, y: self.y},
            n => panic!("Unknown direction {}", n),
        }
    }
}

fn opposite_direction(direction: intcode::IntCode) -> intcode::IntCode {
    match direction {
        NORTH => SOUTH,
        SOUTH => NORTH,
        EAST => WEST,
        WEST => EAST,
        n => panic!("Unknown direction {}", n),
    }
}

#[derive(Clone, Copy, Debug)]
enum Tile {
    Wall,
    Open(usize),
    Oxygen(usize),
}

impl Tile {
    fn get_value(&self) -> Option<usize> {
        match self {
            Tile::Wall => None,
            Tile::Open(n) => Some(*n),
            Tile::Oxygen(n) => Some(*n),
        }
    }

    fn is_open(&self) -> bool {
        match self {
            Tile::Open(_) => true,
            _ => false,
        }
    }

    fn is_oxygen(&self) -> bool {
        match self {
            Tile::Oxygen(_) => true,
            _ => false,
        }
    }
}

fn explore(mut program: &mut intcode::State,
           pos: &Position,
           mut map: &mut Map,
           dist: usize)
{
    for dir in [NORTH, SOUTH, EAST, WEST].iter() {
        if !map.contains_key(&pos.neighbour(*dir)) {
            match program.process(Some(*dir)).unwrap() {
                WALL => {
                    // Did not move
                    map.insert(pos.neighbour(*dir), Tile::Wall);
                }
                OPEN => {
                    map.insert(pos.neighbour(*dir), Tile::Open(dist + 1));
                    explore(&mut program,
                            &pos.neighbour(*dir),
                            &mut map,
                            dist + 1);
                    program.process(Some(opposite_direction(*dir)));
                }
                OXYGEN => {
                    map.insert(pos.neighbour(*dir), Tile::Oxygen(dist + 1));
                    // TODO: recurse here
                    program.process(Some(opposite_direction(*dir)));
                }
                n => panic!("Unexpected status code {}!", n),
            }
        }
    }
}

fn print_debug_map(map: &Map, pos: &Position) {
    let min_x = map.keys().map(|k| k.x).min().unwrap();
    let max_x = map.keys().map(|k| k.x).max().unwrap();
    let min_y = map.keys().map(|k| k.y).min().unwrap();
    let max_y = map.keys().map(|k| k.y).max().unwrap();

    for y in min_y..(max_y + 1) {
        for x in min_x..(max_x + 1) {
            if pos.x == x && pos.y == y {
                print!("D");
                continue;
            }
            print!("{}", match map.get(&Position {x, y}) {
                None => ' ',
                Some(Tile::Wall) => '#',
                Some(Tile::Open(_)) => '.',
                Some(Tile::Oxygen(_)) => 'O',
            });
        }
        println!("");
    }
}

fn fill(mut map: Map) -> usize {
    let mut epochs = 0;
    while map.values().filter(|v| v.is_open()).count() > 0 {
        map = map.iter().map(|(k, v)|
            match v {
                Tile::Wall | Tile::Oxygen(_) => (*k, *v),
                Tile::Open(_) => {
                    if map.get(&k.neighbour(NORTH)).unwrap().is_oxygen() ||
                        map.get(&k.neighbour(SOUTH)).unwrap().is_oxygen() ||
                        map.get(&k.neighbour(EAST)).unwrap().is_oxygen() ||
                        map.get(&k.neighbour(WEST)).unwrap().is_oxygen()
                    {
                        (*k, Tile::Oxygen(0))
                    } else {
                        (*k, *v)
                    }
                }
            })
            .collect::<Map>();
        epochs += 1;
    }
    epochs
}

fn main() {
    let mut program = intcode::State::new(&intcode::read_from_stdin());
    let mut map: Map = HashMap::new();
    let pos: Position = Position { x: 0, y: 0 };

    map.insert(pos, Tile::Open(0));

    explore(&mut program, &pos, &mut map, 0);

    println!("Day 15 part 1: {:?}",
             map.values()
                .filter(|v| v.is_oxygen())
                .nth(0).unwrap().get_value().unwrap());
    println!("Day 15 part 2: {:?}", fill(map));
}
