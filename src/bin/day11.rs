use std::collections::HashMap;

mod intcode;

type Position = (intcode::IntCode, intcode::IntCode);

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn turn(direction: &Direction, action: &intcode::IntCode)
        -> Direction
    {
        match direction {
            Direction::Up    => if *action == 0 { Direction::Left }
                else { Direction::Right },
            Direction::Down  => if *action == 0 { Direction::Right }
                else { Direction::Left },
            Direction::Left  => if *action == 0 { Direction::Down }
                else { Direction::Up },
            Direction::Right => if *action == 0 { Direction::Up }
                else { Direction::Down },
        }
    }

    fn action(direction: &Direction, position: Position) -> Position {
        match direction {
            Direction::Up    => (position.0, position.1 - 1),
            Direction::Down  => (position.0, position.1 + 1),
            Direction::Left  => (position.0 - 1, position.1),
            Direction::Right => (position.0 + 1, position.1),
        }
    }
}

fn main() {
    let mut program = intcode::State::new(&intcode::read_from_stdin());
    let mut hull: HashMap<Position, intcode::IntCode> = HashMap::new();
    let mut position: Position = (0, 0);
    let mut direction = Direction::Up;

    let mut part2 = program.clone();

    loop {
        match program.state {
            intcode::RunningState::Done => break,
            _ => {}
        }

        let paint = hull.entry(position).or_insert(0);
        program.process(Some(*paint));
        *paint = program.outputs[0];
        direction = Direction::turn(&direction, &program.outputs[1]);
        position = Direction::action(&direction, position);
        program.reset_output();
    }
    println!("Day 11 part 1: {}", hull.len());

    let mut hull: HashMap<Position, intcode::IntCode> = HashMap::new();
    let mut position: Position = (0, 0);
    let mut direction = Direction::Up;

    hull.insert((0, 0), 1);

    loop {
        match part2.state {
            intcode::RunningState::Done => break,
            _ => {}
        }
        let paint = hull.entry(position).or_insert(0);
        part2.process(Some(*paint));
        *paint = part2.outputs[0];
        direction = Direction::turn(&direction, &part2.outputs[1]);
        position = Direction::action(&direction, position);
        part2.reset_output();
    }

    let min_x = hull.keys().map(|k| k.0).min().unwrap();
    let max_x = hull.keys().map(|k| k.0).max().unwrap();
    let min_y = hull.keys().map(|k| k.1).min().unwrap();
    let max_y = hull.keys().map(|k| k.1).max().unwrap();

    for y in min_y..max_y+1 {
        for x in min_x..max_x+1 {
            print!("{}", match hull.get(&(x, y)) {
                Some(1) => '#',
                Some(_) => ' ',
                None => ' ',
            });
        }
        println!("");
    }
}
