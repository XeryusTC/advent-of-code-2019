use std::collections::HashSet;
use std::io::{self, BufRead};

#[derive(Debug)]
enum Path {
    Up(i32),
    Down(i32),
    Left(i32),
    Right(i32),
}

type Wire = HashSet<(i32, i32)>;

fn to_coordinates(path: &Vec<Path>) -> Wire {
    let mut x = 0;
    let mut y = 0;
    let mut wire = Wire::new();
    for p in path {
        let dx: i32;
        let dy: i32;
        let steps: i32;
        match p {
            Path::Up(n)    => { dx= 0; dy=-1; steps=*n; }
            Path::Down(n)  => { dx= 0; dy= 1; steps=*n; }
            Path::Left(n)  => { dx= 1; dy= 0; steps=*n; }
            Path::Right(n) => { dx=-1; dy= 0; steps=*n; }
        }

        for _ in 0..steps {
            x += dx;
            y += dy;
            wire.insert((x, y));
        }
    }
    wire
}

fn steps_taken(path: &Vec<Path>, target: &(i32, i32)) -> i32 {
    let mut x = 0;
    let mut y = 0;
    let mut nx;
    let mut ny;
    let mut length = 0;
    for p in path {
        match p {
            Path::Up(n)    => { nx = x; ny = y - n; }
            Path::Down(n)  => { nx = x; ny = y + n; }
            Path::Left(n)  => { nx = x + n; ny = y; }
            Path::Right(n) => { nx = x - n; ny = y; }
        }
        let sx = if x <  nx { x } else { nx };
        let lx = if x >= nx { x } else { nx };
        let sy = if y <  ny { y } else { ny };
        let ly = if y >= ny { y } else { ny };
        if target.0 >= sx && target.0 <= lx
            && target.1 >= sy && target.1 <= ly {
            length += (target.0 - x).abs() + (target.1 - y).abs();
            break;
        } else {
            x = nx;
            y = ny;
            length += (lx - sx) + (ly - sy);
        }
    }
    length
}

fn main() {
    let stdin = io::stdin();
    let paths: Vec<Vec<Path>> = stdin.lock().lines()
        .map(|l| l.unwrap().split(',')
             .map(|s| {
                 let mut s = s.chars();
                 let fst = s.next().unwrap();
                 let num: i32 = s.collect::<String>().parse::<i32>().unwrap();
                 match fst {
                     'U' => { Path::Up(num) }
                     'D' => { Path::Down(num) }
                     'L' => { Path::Left(num) }
                     'R' => { Path::Right(num) }
                     x => panic!("Unknown direction {}", x),
                 }
             })
             .collect())
        .collect();
    let wires: Vec<Wire> = paths.iter().map(to_coordinates).collect();
    
    // Part 1
    let crossings = wires[0].intersection(&wires[1]);
    let result: i32 = crossings.clone().map(|(x, y)| x.abs() + y.abs())
        .min().unwrap();
    println!("Day 3 part 1: {}", result);

    // Part 2
    let result: i32 = crossings.map(|c| paths.iter()
                               .map(|p| steps_taken(p, c)).sum())
        .min().unwrap();
    println!("Day 3 part 2: {}", result);
}
