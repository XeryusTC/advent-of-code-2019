const MIN: usize = 136818;
const MAX: usize = 685979;

fn split_num(mut n: usize) -> Vec<usize> {
    const B: usize = 10;
    let mut split = Vec::new();
    while n > 0 {
        split.push(n % B);
        n /= 10;
    }
    split.reverse();
    split
}

fn is_increasing(n: &Vec<usize>) -> bool {
    n.iter().zip(n.iter().skip(1))
        .all(|(a, b)| a <= b)
}

fn has_double(n: &Vec<usize>) -> bool {
    let mut n = n.to_vec();
    n.dedup();
    n.len() <= 5
}

fn single_double(n: &Vec<usize>) -> bool {
    let mut last = n[0];
    let mut rle = vec![];
    let mut run = 0;
    for num in n {
        if *num == last {
            run += 1;
        } else {
            rle.push(run);
            last = *num;
            run = 1;
        }
    }
    rle.push(run);
    rle.contains(&2)
}

fn main() {
    let nums = (MIN..MAX).map(split_num)
        .filter(is_increasing);
    let result = nums.clone()
        .filter(has_double)
        .collect::<Vec<Vec<usize>>>().len();
    println!("Day 4 part 1: {:?}", result);

    let result = nums
        .filter(single_double)
        .collect::<Vec<Vec<usize>>>().len();
    println!("Day 4 part 2: {:?}", result);
}
