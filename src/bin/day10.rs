use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};
use std::f32::consts::{PI, FRAC_PI_2};

type Coord = (isize, isize);

#[derive(Debug, Eq, PartialEq, Hash)]
struct Ratio {
    x: isize,
    y: isize,
}

impl Ratio {
    fn new(x: isize, y: isize) -> Ratio {
        let gcd = Ratio::gcd(x, y);
        let x = x / gcd;
        let y = y / gcd;
        Ratio { x, y }
    }

    fn gcd(mut m: isize, mut n: isize) -> isize {
        while m != 0 {
            let old_m = m;
            m = n % m;
            n = old_m;
        }
        n.abs()
    }
}

#[derive(Clone, Debug)]
struct Asteroid {
    coord: Coord,
    angle: f32,
    distance: f32,
    destroyed: bool,
}

impl Asteroid {
    fn new(coord: Coord, angle: f32, distance: f32) -> Asteroid {
        Asteroid { coord, angle, distance, destroyed: false }
    }
}

fn main() {
    let stdin = io::stdin();
    let coords = stdin.lock().lines()
        .map(|l| l.unwrap())
        .enumerate()
        .flat_map(|(y, l)| l.chars().enumerate()
                    .filter_map(|(x, c)| match c {
                        '#' => Some((x as isize, y as isize)),
                        _   => None,
                    })
                    .collect::<Vec<Coord>>()
        )
        .collect::<Vec<Coord>>();

    let visible: HashMap<Coord, HashSet<Ratio>> = coords.iter()
        .map(|c1|
             (*c1, coords.iter()
              .filter(|c2| c1 != *c2)
              .map(|c2| (Ratio::new(c1.0 - c2.0, c1.1 - c2.1)))
              .collect::<HashSet<Ratio>>()
             )
        ).collect();
    let part1 = visible.iter().map(|(_, v)| v.len()).max().unwrap();
    println!("Day 10 part 1: {}", part1);

    let coord: Coord = *visible.iter().max_by_key(|(_, v)| v.len()).unwrap().0;
    let mut asteroids: Vec<Asteroid> = coords.iter()
        .filter(|c| **c != coord)
        .map(|c| {
            let dx = coord.0 as f32 - c.0 as f32;
            let dy = coord.1 as f32 - c.1 as f32;
            Asteroid::new(*c,
                         (dy.atan2(dx) + 2.0 * PI) % (2.0 * PI),
                         (dx*dx + dy*dy).sqrt())
        })
        .collect();

    let mut angles: Vec<f32> = asteroids.iter().map(|a| a.angle).collect();
    angles.sort_by(|a, b| a.partial_cmp(b).unwrap());
    angles.dedup();

    let mut last_asteroid: Option<Asteroid> = None;
    let mut angle_id = angles.iter().enumerate()
        .filter(|(_, a)| **a <= FRAC_PI_2 + 0.0001).last().unwrap().0;
    loop {
        let destroyed = asteroids.iter().filter(|a| a.destroyed).count();
        if destroyed == 200 {
            break;
        }
        let mut roids = asteroids.iter_mut()
            .filter(|a| !a.destroyed && a.angle == angles[angle_id])
            .collect::<Vec<&mut Asteroid>>();
        roids.sort_by(|a, b| a.distance.partial_cmp(&b.distance).unwrap());
        if roids.len() > 0 {
            roids[0].destroyed = true;
            last_asteroid = Some(roids[0].clone());
        }

        angle_id = (angle_id + angles.len() + 1) % angles.len();
    }
    let last_asteroid = last_asteroid.unwrap();
    println!("Day 10 part 2: {}",
             last_asteroid.coord.0 * 100 + last_asteroid.coord.1);
}
