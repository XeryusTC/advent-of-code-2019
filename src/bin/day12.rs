use std::io::{self, BufRead};

const EPOCHS: usize = 1000;

#[derive(Clone, Copy, Debug)]
struct Moon {
    x: isize,
    y: isize,
    z: isize,
    vx: isize,
    vy: isize,
    vz: isize,
}

impl Moon {
    fn new(pos: Vec<isize>) -> Moon {
        Moon {
            x: pos[0],
            y: pos[1],
            z: pos[2],
            vx: 0,
            vy: 0,
            vz: 0,
        }
    }

    fn gravity(&self, other: &Moon) -> Moon {
        Moon {
            x: self.x,
            y: self.y,
            z: self.z,
            vx: self.vx - (self.x - other.x).max(-1).min(1),
            vy: self.vy - (self.y - other.y).max(-1).min(1),
            vz: self.vz - (self.z - other.z).max(-1).min(1),
        }
    }

    fn velocity(self) -> Moon {
        Moon {
            x: self.x + self.vx,
            y: self.y + self.vy,
            z: self.z + self.vz,
            ..self
        }
    }

    fn energy(&self) -> isize {
        (self.x.abs() + self.y.abs() + self.z.abs()) *
            (self.vx.abs() + self.vy.abs() + self.vz.abs())
    }
}

fn main() {
    let stdin = io::stdin();
    let mut moons = stdin.lock().lines()
        .map(|l| Moon::new(l.unwrap().split(',')
             .map(|pos| pos.chars().filter(|c| c.is_numeric() || *c == '-')
                  .collect::<String>().parse::<isize>().unwrap())
             .collect::<Vec<isize>>()))
        .collect::<Vec<Moon>>();

    for _ in 0..EPOCHS {
        moons = moons.iter()//.map(|m| m.reset_velocity())
            .map(|m| moons.iter().fold(*m, |acc, m2| acc.gravity(m2)))
            .map(|m| m.velocity())
            .collect::<Vec<Moon>>();
    }

    let energy: isize = moons.iter().map(|m| m.energy()).sum();
    println!("Day 12 part 1: {}", energy);
}
