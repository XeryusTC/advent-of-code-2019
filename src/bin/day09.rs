mod intcode;

fn main() {
    let mut program = intcode::read_from_stdin();
    let output = intcode::process(&mut program, &mut vec![1]);
    println!("Day 9 part 1: {}", output.unwrap());
    let output = intcode::process(&mut program, &mut vec![2]);
    println!("Day 9 part 2: {}", output.unwrap());
}
