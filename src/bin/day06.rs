use std::collections::{HashMap, HashSet, VecDeque};
use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let defs: Vec<Vec<String>> = stdin.lock().lines()
        .map(|l| l.unwrap().split(')')
             .map(|o| o.to_string())
             .collect())
        .collect();
    let mut orbits: HashMap<String, usize> = HashMap::new();
    orbits.insert("COM".to_string(), 0);

    let mut changed = true;
    while changed {
        changed = false;
        for def in defs.iter() {
            let parent = &def[0];
            let child = &def[1];
            if orbits.contains_key(parent) && !orbits.contains_key(child) {
                changed = true;
                orbits.insert(def[1].clone(), orbits.get(parent).unwrap() + 1);
            }
        }
    }

    let result: usize = orbits.iter().map(|(_, v)| *v).sum();
    println!("Day 6 part 1: {}", result);

    let mut closed: HashSet<String> = HashSet::new();
    let mut open: VecDeque<(i32, String)> = VecDeque::new();
    open.push_back((0, "YOU".to_string()));
    loop {
        match open.pop_front() {
            None => break,
            Some((cost, node)) => {
                if node == "SAN".to_string() {
                    println!("Day 6 part 2: {}", cost - 2);
                    return;
                }
                closed.insert(node.clone());
                for def in defs.iter() {
                    let parent = &def[0];
                    let child = &def[1];
                    if *parent == node && !closed.contains(child) {
                        open.push_back((cost + 1, child.clone()))
                    }
                    if *child == node && !closed.contains(parent) {
                        open.push_back((cost + 1, parent.clone()))
                    }
                }
            }
        }
    }
    panic!("Failed to find a path");
}
