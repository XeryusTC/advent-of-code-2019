use std::cmp::max;
use std::collections::HashMap;
use std::io::{self, BufRead};

const TRILLION: usize = 1_000_000_000_000;

#[derive(Debug, PartialEq, Eq)]
struct Agent {
    quantity: usize,
    chemical: String,
}

impl Agent {
    fn from_string(raw: &str) -> Agent {
        let mut s = raw.split(" ");
        Agent {
            quantity: s.next().unwrap().parse().unwrap(),
            chemical: s.next().unwrap().to_string(),
        }
    }
}

#[derive(Debug)]
struct Reaction {
    result: Agent,
    reagents: Vec<Agent>,
}

fn is_satisfied(requirements: &HashMap<String, usize>,
                satisfied: &HashMap<String, usize>) -> bool
{
    requirements.iter().all(|(k, v)| match satisfied.get(k) {
        None => k == "ORE",
        Some(satis) => satis >= v,
    })
}

fn satisfy(
    reactions: &HashMap<String, Reaction>,
    requirements: &mut HashMap<String, usize>,
    satisfied: &mut HashMap<String, usize>,
) {
    while !is_satisfied(&requirements, &satisfied) {
        for (chemical, requirement) in requirements.clone().iter() {
            if chemical == "ORE" {
                continue;
            }
            let present = satisfied.entry(chemical.clone()).or_insert(0);
            let reaction = reactions.get(chemical).unwrap();
            if *present < *requirement {
                let amount = max(
                    (*requirement - *present) / reaction.result.quantity,
                    1);
                for reagent in reaction.reagents.iter() {
                    let entry = requirements.entry(reagent.chemical.clone())
                        .or_insert(0);
                    *entry += reagent.quantity * amount;
                }
                let entry = satisfied.entry(reaction.result.chemical.clone())
                    .or_insert(0);
                *entry += reaction.result.quantity * amount;
            }
        }
    }
}

fn main() {
    let stdin = io::stdin();
    let reactions = stdin.lock().lines()
        .map(|l| {
            let parts = l.unwrap().split("=>")
                .map(|l| l.trim().to_string()).collect::<Vec<String>>();
            Reaction {
                result: Agent::from_string(&parts[1]),
                reagents: parts[0]
                    .split(',').map(|p| Agent::from_string(p.trim()))
                    .collect(),

            }
        })
        .map(|r| (r.result.chemical.clone(), r))
        .collect::<HashMap<String, Reaction>>();

    let mut requirements: HashMap<String, usize> = HashMap::new();
    let mut satisfied: HashMap<String, usize> = HashMap::new();
    requirements.insert("FUEL".to_string(), 1);

    satisfy(&reactions, &mut requirements, &mut satisfied);
    let ore_per_fuel = requirements.get("ORE").unwrap().clone();
    println!("Day 14 part 1: {}", ore_per_fuel);

    let mut ore_used = requirements.get("ORE").unwrap().clone();
    while ore_used <= TRILLION {
        requirements.entry("FUEL".to_string())
            .and_modify(|v| *v += max(1, (TRILLION - ore_used) / ore_per_fuel))
            .or_insert(1);
        satisfy(&reactions, &mut requirements, &mut satisfied);
        ore_used = requirements.get("ORE").unwrap().clone();
    }
    println!("Day 14 part 2: {}", requirements.get("FUEL").unwrap() - 1);
}
