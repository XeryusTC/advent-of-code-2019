use std::io::{self, BufRead};

const WIDTH: usize = 25;
const HEIGHT: usize = 6;

fn split_layers(input: String, size: usize) -> Vec<String> {
    let layer_count = input.len() / size;
    let mut layers = Vec::new();
    for i in 0..layer_count {
        layers.push(input[(i * size)..((i + 1) * size)].to_string());
    }
    layers
}

fn main() {
    let stdin = io::stdin();
    let data = split_layers(stdin.lock().lines().next().unwrap().unwrap(),
                            WIDTH * HEIGHT);

    let min = data.iter()
        .map(|l| l.chars().filter(|c| *c == '0').count())
        .zip(data.iter()
            .map(|l| l.chars().filter(|c| *c == '1').count() *
                 l.chars().filter(|c| *c == '2').count()))
        .min_by(|a, b| a.0.cmp(&b.0)).unwrap();
    println!("Day 8 part 1: {}", min.1);

    let mut image: String = "2".repeat(WIDTH * HEIGHT);
    for layer in data.iter() {
        image = image.chars().zip(layer.chars())
            .map(|(i, l)| if i == '2' { l } else { i })
            .collect();
    }

    image = image.chars().map(|c| if c == '0' { ' ' } else { 'X' }).collect();

    println!("Day 8 part 2:");
    for i in 0..HEIGHT {
        println!("{}", image[(i * WIDTH)..((i + 1) * WIDTH)].to_string());
    }
} 
